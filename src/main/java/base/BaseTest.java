package base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import steps.BaseSteps;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    protected BaseSteps baseSteps = new BaseSteps();

    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        SelenideLogger.addListener("AllureSelenide",
                new AllureSelenide()
                        .screenshots(true)
                        .savePageSource(false));
        Configuration.timeout = 10000;
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
//        Configuration.startMaximized = true;
        open("https://www.saucedemo.com/");
    }

    @AfterClass(alwaysRun = true)
    public static void tearDown() {
        Selenide.close();
    }

    protected void authorization() {
        baseSteps.openStartPage();
    }

}
