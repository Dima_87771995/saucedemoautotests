package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginPageForm {

    /**
     * Поле: Логин
     *
     * @return
     */
    public SelenideElement loginField() {
        return $(By.id("user-name"));
    }

    /**
     * Поле: Пароль
     *
     * @return
     */
    public SelenideElement passwField() {
        return $(By.name("password"));
    }

    /**
     * Кнопка: Логин
     *
     * @return
     */
    public SelenideElement loginButton() {
        return $(By.cssSelector("input#login-button.submit-button.btn_action"));
    }

    /**
     * Иконка: Робот
     *
     * @return
     */
    public SelenideElement iconRobot() {
        return $(By.cssSelector("div.bot_column"));
    }

    /**
     * Логотип: Логин
     *
     * @return
     */
    public SelenideElement loginLogo() {
        return $(By.cssSelector("div.login_logo"));
    }

    /**
     * Сообщение: Ошибка
     *
     * @return
     */
    public SelenideElement errorMessage() {
        return $(By.cssSelector("div.error-message-container.error"));
    }
}
