package models;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ClientOperations {
    private String username;
    private String password;
    private String lockedMessage;
    private String errorMessage;
}
