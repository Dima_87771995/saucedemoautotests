package steps;

import elements.LoginPageForm;
import io.qameta.allure.Step;
import models.ClientOperations;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

public class LoginPageSteps {

    LoginPageForm loginPageForm = new LoginPageForm();

    @Step("Проверяем что главная страница открылась")
    public void shouldBeMainLoginPage() {
        loginPageForm.loginLogo().waitUntil(visible, 3000).shouldBe(visible);
        loginPageForm.iconRobot().shouldBe(visible);
    }

    @Step("Авторизация по логину '{clientOperations.username}' и паролю '{clientOperations.password}'")
    public void authorizationByLogAndPassw(ClientOperations clientOperations) {
        loginPageForm.loginField().setValue(clientOperations.getUsername());
        loginPageForm.passwField().setValue(clientOperations.getPassword());
    }

    @Step("Авторизация по логину '{clientOperations.username}' и паролю '{clientOperations.password}'")
    public void aauthorizationByLogAndPassw(ClientOperations clientOperations) {
        loginPageForm.loginField().setValue(clientOperations.getUsername());
        loginPageForm.passwField().setValue(clientOperations.getPassword());
    }

    @Step("Вводим логин '{clientOperations.username}'")
    public void writeLogin(ClientOperations clientOperations) {
        loginPageForm.loginField().setValue(clientOperations.getUsername());
    }

    @Step("Вводим пароль '{clientOperations.password}'")
    public void writePassword(ClientOperations clientOperations) {
        loginPageForm.passwField().setValue(clientOperations.getPassword());
    }

    @Step("Кликаем по кнопке логин")
    public void clickLoginButton() {
        loginPageForm.loginButton().waitUntil(visible, 3000).click();
    }

    @Step("Проверяем сообщение об ошибке '{clientOperations.lockedMessage}' для закрытого пользователя")
    public void verifyLockedMessage(ClientOperations clientOperations) {
        loginPageForm.errorMessage()
                .waitUntil(visible, 3000)
                .shouldHave(text(clientOperations.getLockedMessage()));
    }

    @Step("Проверяем сообщение об ошибке '{clientOperations.errorMessage}'")
    public void verifyErrorMessage(ClientOperations clientOperations) {
        loginPageForm.errorMessage()
                .waitUntil(visible, 3000)
                .shouldHave(text(clientOperations.getErrorMessage()));
    }
}
