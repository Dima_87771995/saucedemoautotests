package steps;

import com.codeborne.selenide.Selenide;
import config.PropertiesConfig;
import io.qameta.allure.Step;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Iterator;
import java.util.logging.Logger;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BaseSteps {

    PropertiesConfig config = ConfigFactory.create(PropertiesConfig.class);
    private static final Logger LOGGER = Logger.getLogger(BaseSteps.class.getName());


    @Step("Открытие стартовой страницы")
    private void openStartPage(String url) {
        Selenide.clearBrowserCookies();
        System.out.println(url);
        try {
            Selenide.open(url);
        } catch (org.openqa.selenium.TimeoutException e) {
            LOGGER.warning("Browser failed to open!");
        }
    }

    @Step("Открытие стартовой страницы")
    public void openStartPage() {
        System.out.println("url: " + config.stage_url());
        System.out.println("login: " + config.carrier_login());
        openStartPage(config.stage_url());
    }

    @Step("Открытие новой вкладки")
    public void openNewTab() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.open('about:blank','_blank');");
        selectTab(1);
        Selenide.sleep(1000);
    }

    @Step("Выбор следующей вкладки браузера")
    public void selectTab() {
        String handle = getWebDriver().getWindowHandles().iterator().next();
        getWebDriver().switchTo().window(handle);
    }

    @Step("Выбор вкладки браузера №{0}")
    public void selectTab(int number) {
        String handle = (String) (getWebDriver().getWindowHandles().toArray())[number];
        getWebDriver().switchTo().window(handle);
    }

    @Step("Закрытие последней вкладки")
    public void closeLastTab() {
        Iterator<String> handles = getWebDriver().getWindowHandles().iterator();
        String handle = handles.next();
        while (handles.hasNext()) {
            handle = handles.next();
        }
        getWebDriver().switchTo().window(handle);
        getWebDriver().close();
    }

    @Step("Нажатие кнопки 'Назад' в браузере")
    public void pressBackButton() {
        getWebDriver().navigate().back();
    }

}
