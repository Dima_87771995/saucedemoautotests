package config;

import org.aeonbits.owner.Config;

@Config.Sources("file:src/main/resources/test.properties")
public interface PropertiesConfig extends Config {

    @Config.Key("stage.url")
    String stage_url();

    @Config.Key("qa.url")
    String qa_url();

    @Config.Key("customer.login")
    String customer_login();

    @Config.Key("customer.password")
    String customer_password();

    @Config.Key("carrier.login")
    String carrier_login();

    @Config.Key("carrier.password")
    String carrier_password();

}

