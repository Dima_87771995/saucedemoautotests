package smoke;

import base.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import models.ClientOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.LoginPageSteps;

@Feature("Web login")
public class LockedOutUserLoginTest extends BaseTest {

    LoginPageSteps loginPageSteps = new LoginPageSteps();
    ClientOperations clientOperations = new ClientOperations();

    @BeforeClass(alwaysRun = true)
    public void preconditions() {
        clientOperations
                .setUsername("locked_out_user")
                .setPassword("secret_sauce")
//                .setLockedMessage("Epic sadface: Sorry, this user has been locked out.");
                .setLockedMessage("Epic sadface: Sorry, this user has been locked ot.");
    }

    @Test(groups = {"smoke", "smoke.Login2", "TC-74135"})
    @TmsLink("TC-74135")
    @Story("Тест на авторизацию по логину и паролю с использованием закрытого пользователя")
    public void lockedOutUserLoginTest() {
        loginPageSteps.shouldBeMainLoginPage();
        loginPageSteps.authorizationByLogAndPassw(clientOperations);
        loginPageSteps.clickLoginButton();
        loginPageSteps.verifyLockedMessage(clientOperations);
    }
}
