package smoke;

import base.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import models.ClientOperations;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.LoginPageSteps;

@Feature("Web login")
public class StandardUserLoginTest extends BaseTest {

    LoginPageSteps loginPageSteps = new LoginPageSteps();
    ClientOperations clientOperations = new ClientOperations();

    @BeforeClass(alwaysRun = true)
    public void preconditions() {
        clientOperations
                .setUsername("standard_user")
                .setPassword("secret_sauce");
    }

    @Test(groups = {"smoke", "smoke.Login3", "TC-74134"})
    @TmsLink("TC-74134")
    @Story("Тест на авторизацию по логину и паролю с использованием стандартного пользователя")
    public void standardUserLoginTest() {
        loginPageSteps.shouldBeMainLoginPage();
        loginPageSteps.authorizationByLogAndPassw(clientOperations);
        loginPageSteps.clickLoginButton();
    }
}
