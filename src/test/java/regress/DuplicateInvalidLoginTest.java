package regress;

import base.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import models.ClientOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.LoginPageSteps;

@Feature("Web login")
public class DuplicateInvalidLoginTest extends BaseTest {

    LoginPageSteps loginPageSteps = new LoginPageSteps();
    ClientOperations clientOperations = new ClientOperations();

    @BeforeClass(alwaysRun = true)
    public void preconditions() {

        clientOperations
                .setUsername("Another_user")
                .setPassword("secret_sauce")
                .setErrorMessage("Epic sadface: Username and password do not match any user in this service");
    }

    @Test(groups = {"regress", "TC-74146", "regress.Login1"}, dependsOnGroups = {"smoke.Login1"})
    @TmsLink("TC-74146")
    @Story("Тест на авторизацию с импользованием не существующего пользователя дубликат")
    public void duplicateInvalidLoginTest() {
        loginPageSteps.shouldBeMainLoginPage();
        loginPageSteps.writeLogin(clientOperations);
        loginPageSteps.writePassword(clientOperations);
        loginPageSteps.clickLoginButton();
        loginPageSteps.verifyErrorMessage(clientOperations);
    }
}