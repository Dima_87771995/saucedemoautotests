package regress;

import base.BaseTest;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import models.ClientOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.LoginPageSteps;

public class DuplicateStandardUserLoginTest extends BaseTest {

    LoginPageSteps loginPageSteps = new LoginPageSteps();
    ClientOperations clientOperations = new ClientOperations();

    @BeforeClass(alwaysRun = true)
    public void preconditions() {
        clientOperations
                .setUsername("standard_user")
                .setPassword("secret_sauce");
    }

    @Test(groups = {"regress", "TC-74144", "regress.Login3"}, dependsOnGroups = {"smoke.Login3"})
    @TmsLink("TC-74144")
    @Story("Тест на авторизацию по логину и паролю с использованием стандартного пользователя дубликат")
    public void duplicateStandardUserLoginTest() {
        loginPageSteps.shouldBeMainLoginPage();
        loginPageSteps.authorizationByLogAndPassw(clientOperations);
        loginPageSteps.clickLoginButton();
    }
}