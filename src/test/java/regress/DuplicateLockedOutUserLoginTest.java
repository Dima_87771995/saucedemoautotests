package regress;

import base.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import models.ClientOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.LoginPageSteps;


@Feature("Web login")
public class DuplicateLockedOutUserLoginTest extends BaseTest {

    LoginPageSteps loginPageSteps = new LoginPageSteps();
    ClientOperations clientOperations = new ClientOperations();

    @BeforeClass(alwaysRun = true)
    public void preconditions() {
        clientOperations
                .setUsername("locked_out_user")
                .setPassword("secret_sauce")
//                .setLockedMessage("Epic sadface: Sorry, this user has been locked out.");
                .setLockedMessage("Epic sadface: Sorry, this user has been locked   d out.");
    }

    @Test(groups = {"regress","TC-74145", "regress.Login2"}, dependsOnGroups = {"smoke.Login2"})
    @TmsLink("TC-74145")
    @Story("Тест на авторизацию по логину и паролю с использованием закрытого пользователя дубликат")
    public void duplicateLockedOutUserLoginTest() {
        loginPageSteps.shouldBeMainLoginPage();
        loginPageSteps.authorizationByLogAndPassw(clientOperations);
        loginPageSteps.clickLoginButton();
        loginPageSteps.verifyLockedMessage(clientOperations);
    }
}