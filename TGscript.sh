java \
"-DprojectName=SauseDemoProject" \
"-Denv=QA02" \
"-DreportLink=${CI_PIPELINE_URL}" \
"-Dcomm=${CI_COMMIT_BRANCH}" \
"-Dconfig.file=./allure-notifications.json" \
-jar allure-notifications-3.1.2.jar
