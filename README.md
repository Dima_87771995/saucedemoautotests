# SaucedemoAutotests

mvn clean install -Dgroups = {"group"} - Запуск определенной группы тестов smoke или regress
mvn clean test -DthreadCount=n - Параллельный запуск тестов количеством n

Запуск течтов используя Docker:
1) Запуск Selenium Server :
docker run -d --rm -p 4444:4444 selenium/standalone-chrome:3.141.59
2) Запуск контейнера с тестами :
docker run --rm --network host --mount type=bind,src=C:\MarketTestSelenium\saucedemoautotests,target=/usr/src/tests -w /usr/src/tests maven:3.6.3-ibmjava-8 mvn clean test 

Запуск тестов в Selenium server локально:
Добавить ключ -Dselenide.remote=http://localhost:4444/wd/hub

docker run --rm --network host --mount type=bind,src=C:\MarketTestSelenium\saucedemoautotests,target=/usr/src/tests --mount type=volume,src=allure-results,target=/usr/src/tests -w /usr/src/tests maven:3.6.3-ibmjava-8 mvn clean test

